
#ifndef FORHSH_h
#define FORHSH_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include "hashMap.h"
#include "utils.h"

/******************************************************************************
 *                                execute                                     *
 ******************************************************************************/

void execute(char string[], hashMap *dict, int *status);

/******************************************************************************
 *                                forloop                                     *
 ******************************************************************************/

void forloop(char *stringg, hashMap *dict, int *status);

/******************************************************************************
 *                           checkExitStatus                                  *
 ******************************************************************************/

void checkExitStatus(char *stringg, hashMap *dict, int *status);

#endif // FORHSH_h