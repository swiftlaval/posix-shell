/**
 * @file utils.h
 * @brief Contains various static utility functions
 *
 * Functions that help at parsing commandline, manipulating the current working
 * directory and printing debugging info.
 *
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 */

#ifndef HASHUP_UTILS_H
#define HASHUP_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include "hashMap.h"
#include "error_codes.h"
#include "forhsh.h"

#ifdef DEBUG

/******************************************************************************
 *                                  print DEBUG                               *
 ******************************************************************************/
/**
 * @brief Prints to stdout a split commandline.
 * @param argv [char**] c-string containing a split commandline
 * @return void
 */
static void print(char **argv) {
  int i = 0;
  while (!(argv[i] == NULL)) {
    printf("%s %d \n", argv[i], i);
    i++;
  }
  fflush(stdout);
}
#endif // DEBUG

/******************************************************************************
 *                                  parse                                     *
 ******************************************************************************/

/**
 * @brief This is used to parse the commandline into an array of c-strings
 * describing commands with their options.
 * @param buff [char*] c-string containing user's input
 * @param argc [int] length of argv array
 * @param argv [char**] array containing a split commandline
 * @note argv[0] is command name and argv ends with NULL
 * @param bgPtr [int*] flag, if commandline is to be processed at background
 * @return 0 if success, else int error code according to syntax error
 */
static int parse(hashMap *dict, char *buff, int *argc, char **argv,
                 int *bgPtr) {

  // printf(">>%s<<\n",buff);
  if (strncmp(buff, "for", 3 * sizeof(char)) != 0 &&
      strncmp(argv[0], "for", 3 * sizeof(char)) != 0) {

    for (int xj = 0; xj < strlen(buff) - 1; xj++) {
      if ((buff[xj] == '&' && buff[xj + 1] == '&') ||
          (buff[xj] == '|' && buff[xj + 1] == '|')) {
        int status = 0;
        checkExitStatus(buff, dict, &status);
        return SUCCESS;
      }
    }
#ifdef DEBUGDICT
    printf("\n Dict at start of parse .");
    printMap(dict);
    printf("\n");
#endif // DEBUGDICT

    //!< split buffer to argv array
    *argc = 0;
    argv[0] = strtok(buff, " \n");
    while (argv[*argc] != NULL && *argc < 5) {
      argv[++(*argc)] = strtok(NULL, " \n");
    }
    //!< check if argv has nothing
    if (argv[0] == NULL)
      return -10; // cmd is a newline
    //!< check if argv ends with background-detached symbol
    if (strcmp(argv[*argc - 1], "&") == 0) {
      argv[*argc - 1] = NULL;
      *bgPtr = 1;
      (*argc)--;
    } else {
      argv[*argc] = NULL;
      *bgPtr = 0;
    }
    //!< check if cmd begins inappropriately
    if ((strcmp(argv[0], "&") == 0) || (strcmp(argv[0], ";") == 0))
      return BADTOKEN; // syntax error

  } else {
    int status = 0;
    forloop(buff, dict, &status);
    return SUCCESS;
  }
  //!< check for ~ or ~/ and replace them with homedir
  int i;
  for (i = 0; i < *argc; ++i) {
    if ((strcmp(argv[i], "~") == 0) ||
        (strncmp(argv[i], "~/", 2 * sizeof(char)) == 0)) {
      char *dir = getenv("HOME");
      strcat(dir, argv[i] + 1);
      argv[i] = dir;
    }
  }

  //!< check for variable calls
  for (i = 0; i < *argc; ++i) {
    if ((strncmp(argv[i], "$", sizeof(char)) == 0)) {
      if (containsKey(dict, argv[i] + 1) != 0) {
        char *key_var = (char *)malloc(strlen(atMap(dict, argv[i] + 1)));

        strcat(key_var, atMap(dict, argv[i] + 1));

        argv[i] = key_var;
      } else {
        printf("Variable %s NOT FOUND\n", argv[i] + 1);
      }
#ifdef DEBUGDICT
      printMap(dict);
      printf("Value found in map: %s", key_var);
#endif // DEBUGDICT
    };
  };

  //!< check for variable intialization
  if (*argc == 1) {
    char *p_eq = strchr(argv[0], '=');
    if (p_eq != NULL) {
      char *ele;
      char *val;
      ele = strtok(argv[0], "=");
      val = strtok(NULL, "=");
      if ((strncmp(val, "$", sizeof(char)) == 0)) {
      if (containsKey(dict, val + 1) != 0) {
        char *nargv = (char *)malloc(strlen(atMap(dict, val + 1)));

        strcat(nargv, atMap(dict, argv[i] + 1));
        insertMap(dict, ele, nargv);
      } else {
        printf("Variable %s NOT FOUND\n", val +1);
      }
    }else{
      insertMap(dict, ele, val);
    }
    printMap(dict);
    return SUCCESS;
    }
  };

#ifdef DEBUGDICT
  printf("\n Dict after var ini in code.");
  printMap(dict);
  printf("\n");
#endif // DEBUGDICT

  //!< check if cmd start with a directory or non-existant directory or file
  struct stat st;
  lstat(argv[0], &st);
  if (S_ISDIR(st.st_mode))
    return DIRECTORY; // cmd is directory
  if ((strncmp(argv[0], "/", sizeof(char)) == 0) ||
      (strncmp(argv[0], "./", 2 * sizeof(char)) == 0) ||
      (strncmp(argv[0], "../", 3 * sizeof(char)) == 0)) {
    if (access(argv[0], F_OK))
      return NOFORDIR; // cmd is non-existant file or directory
  }

  return SUCCESS; // success
}

/******************************************************************************
 *                                  outputcwd                                 *
 ******************************************************************************/

/**
 * @brief prints information about current working directory
 * @param cwd [char*] c-string containing the whole cwd
 * @param argv [char**] array of c-strings which will hold split piece of cwd
 * @param buf [char*] c-string that will contain the resulted cwd to be output
 * @return char* c-string containing part of cwd
 */
static char *outputcwd(hashMap *dict, char *cwd, char **argv, char *buf) {

#ifdef DEBUGDICT
  printf("\nDict at start outputcwd:");
  printMap(dict);
  printf("\n");
#endif // DEBUGDICT

  int argc = 0;
  argv[0] = strtok(cwd, "/\0");
  while (argv[argc] != NULL && argc < 64) {
    argv[++argc] = strtok(NULL, "/");
  }

#ifdef DEBUGDICT
  printf("\nDict after while outputcwd:");
  printMap(dict);
  printf("\nHere is buf: %s", buf);
  printf("\n");
#endif // DEBUGDICT

  strcpy(buf, "/\0");
  if (argc > 1) {

#ifdef DEBUGDICT
    printf("\nDict after strcpy outputcwd:");
    printMap(dict);
    printf("\nHere is buf: %s", buf);
    printf("\n");
#endif // DEBUGDICT

    strcat(buf, argv[argc - 2]);

#ifdef DEBUGDICT
    printf("\nDict after 1rst strcat outputcwd:");
    printMap(dict);
    printf("\n");
    printf("Size of buf: %d", buf);
    printf("\nHere is buf: %s", buf);
#endif // DEBUGDICT

    strcat(buf, "/\0");

#ifdef DEBUGDICT
    printf("\n Dict after second strcat outputcwd:");
    printMap(dict);
    printf("\n");
    printf("Size of buf: %d", buf);
    printf("\n Here is buf: %s", buf);
#endif // DEBUGDICT
  }
  fflush(stdout);
  if (argc > 0) {
    strcat(buf, argv[argc - 1]);
  }

#ifdef DEBUGDICT
  printf("\n Dict at end afer flush outputcwd:");
  printMap(dict);
  printf("\n");
#endif // DEBUGDICT
  return buf;
}

#endif // HASHUP_UTILS_H
