/**
 * @file error_codes.h
 * @brief Header file for error_codes_t definition
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 */

#ifndef HASHUP_ERROR_CODES_H
#define HASHUP_ERROR_CODES_H

/**
 * @brief enumeration of error codes returned by hash-up functions in order to
 * avoid magic numbers.
 */
typedef enum {
  SUCCESS = 0,    //!< success
  NOFORDIR = -2,  //!< directory not found
  NOTFOUND = 2,   //!< command not found
  SEGMFAULT = 11, //!< segmentation fault
  NOPERMS = 13,   //!< permission denied
  BADTOKEN,       //!< syntax error due to bad token
  DIRECTORY,      //!< cmd is a directory
  PROCFAIL,       //!< unable to fork process
  NOBUILTIN       //!< no such built-in hashup command
} error_codes_t;

#endif // HASHUP_ERROR_CODES_H
