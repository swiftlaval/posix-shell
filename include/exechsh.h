/**
 * @file exechsh.h
 * @brief Header file for static library of hash-up! built-in commands
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 */

#ifndef HASHUP_EXECHSH_H
#define HASHUP_EXECHSH_H

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "error_codes.h"

/******************************************************************************
 *                                  exechsh                                   *
 ******************************************************************************/

/**
 * @brief Checks if command corresponds to a shell-language keyword and executes
 * it.
 * @param argc [int] length of argv array
 * @param argv [char**] c-string containing a split commandline
 * @return 0 if success, else int error code
 */
int exechsh(int argc, char **argv);

/******************************************************************************
 *                                  hshcd                                     *
 ******************************************************************************/

/**
 * @brief implements cd (change directory)
 * @param argc [int] length of argv array
 * @param argv [char**] array containing a split commandline
 * @return 0 if success, else int error code
 */
static inline int hshcd(int argc, char **argv) {
  int status;
  char *directory;
  if (argc == 1) {
    directory = getenv("HOME");
  } else {
    directory = argv[1];
  }
  status = chdir(directory);
  if (status == -1) {
    return NOFORDIR; // directory not found
  }
  return SUCCESS;
}

/******************************************************************************
 *                                  hshexit                                   *
 ******************************************************************************/

/**
 * @brief implements exit from hashup shell session
 * @param argc [int] length of argv array
 * @param argv [char**] array containing a split commandline
 * @return 0 if success, else int error code
 */
static inline int hshexit(int argc, char **argv) {
  int error_code = 0;
  if (argc > 1)
    error_code = atoi(argv[1]);
  exit(error_code);
}

#endif // HASHUP_EXECHSH_H
