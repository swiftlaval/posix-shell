/**
 * @file hshlib.h
 * @brief Header file for hash-up! static library
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 * @todo keeping and modifying dictionary of local hash-up variables
 * @see exechsh.c
 * @see execbin.c
 */

#ifndef HASHUP_HSHLIB_H
#define HASHUP_HSHLIB_H

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include "error_codes.h"
#include "hashMap.h"

/******************************************************************************
 *                                  exechsh                                   *
 ******************************************************************************/

#include "exechsh.h"

/******************************************************************************
 *                                  execbin                                   *
 ******************************************************************************/
/**
 * @brief Checks if command corresponds to an executable in PATH and executes
 * it.
 * @param argv [char**] c-string containing a split commandline
 * @param bg [int] flag, if commandline is to be processed at background
 * @return 0 if success, else int error code
 */
int execbin(char **argv, int bg);

/******************************************************************************
 *                                 errhandler                                 *
 ******************************************************************************/
/**
 * @brief Handles errors caused when parsing an invalid commandline.
 * @param argv [char**] c-string containing a split commandline
 * @param status [int] returned by parse function
 * @return void
 */
void handle_parse_error(char **argv, int status);

/**
 * @brief Handles errorcodes returned by executing shell commands or executables
 * @note handles also 'command not found'
 * @param argv [char**] c-string containing a split commandline
 * @param status [int] returned by executable or command
 * @return void
 */
void handle_command_error(char **argv, int status);

/******************************************************************************
 *                              signal handlers                               *
 ******************************************************************************/

/**
 * @brief enabled when sigint is caught
 */
static int SIGINTFLAG = 0;

/**
 * @brief custom handler for SIGINT signal for hash-up!
 * @param sig [int] signal number
 * @return void
 */
static void HSH_SIGINT(int sig) {
  SIGINTFLAG = 1;
  printf("Killing all programs!\n");
  signal(SIGINT, HSH_SIGINT);
}

#endif // HASHUP_HSHLIB_H
