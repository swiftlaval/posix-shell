---
#### Abstract:
This project was done for the course of Operating systems given at the University of Montreal by Professor Liam Paull.

This Project's goal is to familiarize ourselves with system programming in an operating system of POSIX style. Its based on the standard C libraries. One of its objectives is to teach us the how to use \<unistd.h\>. For this project we were asked to implement use command lines similar to /bin/sh, like:
    
    > 1.  Basic shell commands
    > 2.  Execution of built-in commands
    > 3.  Execution of system commands using PATH variables
    > 4.  Variable dynamic dictionary
    > 5.  For loop
    > 6.  Logic relations && and || for the execution of processes depending on their exit status.
#### Authors:
- 
    Yammine, Jad
-  Fayjhi, Moncef



#### Introduction


This Project was designed and implemented by Jad Yammine and Moncef
Fayjha. At the start we have to start by understanding how a shell of an
operating system using the POSIX style operates, plus we have to
understand the nature of the commands we had to execute. To that end
learning the \<unistd.h\> library functions was the obvious next step.
After that we started by putting a plan of how our shell should operate,
before starting with the implementation and the other more specific
tasks. The implementation path will be described futher in this report
in a more detailed manner.

installing and execution implementation extra Feature

#### Installion and compilation

Our implementation uses multiple .c files and .h which are present in
the src and include respectively. A makefile is already setup to compile
the whole project. To compile it:

> To compile it:
> 1.  open a terminal at the directory where the makefile is. ( At the directory you would also find the subfolders src and include)

> 2.  write \"make\" (this will generate the executable named hashup)    (in case of any errors use \"make -B\")
>
> To run:
>
> 1.  write \"./hashup\" in the same terminal (at the same directory)
>
About the hashUp shell
================


#### Parsing of command line :

In utils.h file is the function parse which serves to appropriately
process the c-string (buf) entered by the user and received by the fgets
in hashup.c. It starts by verifying if any logic relation dependent on
the exit status is use in the provided command line or if it starts by a
for. Those 2 cases have seprate functions to deal with their parsing and
execution. After that the parse starts by processing the buf in argv
table from c-strings containing words that comprise the buf
sequentially. It checks for variables initialisation and calls
(explained later). It recognizes and alerts which next command will be
executed in the background. What distinguishes it of the bash , is the
handling of orders in case of errorneous orders or orders that do not
make sense. This transforms the symbol   or   / when its at the start
word to be replaced appropriately which the path starting from HOMEDIR.
It also recognizes if the command starts from address (which is
meaningless) and returns the appropriate code. Understand when the first
word means an address or a file but does not exist in the system.

#### Basic shell commands :

The following Basic shell commands are executed direclty from the normal
shell implementation.

1.  echo
2.  cat
3.  ls
4.  man
5.  tail

#### Variables :

In the parse function, if an input with no spaces and of the following
form is given:

>$TEST=bonjour

parse adds TEST as the key and bonjour as the value in a hashMap data
structure. The HashMap data structure is dynamically allocated, its
manages the memory. The HashMap implementation was inspired from a code
written by robertkety on github.

> Since its not the objective of this course to design data structures
> we deemed it ok to reuse code. The Code used form robertkety had some
> dynamic memory allocation issues which we resolved and had to do
> several modifications for it to fit out needs.

Also Parse checks for variable calls in the inputed command-line. The
Variable call need to be of the form (without the \"\"):

> \"$TEST\"

Parse replaces the \"$TEST\" in argv with its value if found in the
hashMap dictionary.

> For exemple: In: \"$TEST=bonjour\" In: \"echo $TEST\" Out: \"bonjour\"

#### Built-in Shell commands :

After processing the command-line calls the exechsh function to test if
the given command corresponds to a built-in hashUp command. A comparison
on argv\[0\] is done to determine the appropriate command of
SHELL\_COMMANDS panel to run. If not found built-in command
corresponding to argv \[0\] is found NOBUILTIN is returned. For now the
implemented commands are:

1.  cd
2.  exit

#### Execution of system commands using PATH variables :

If NOBUILTIN is returned from exechsh, then execbin called. execbin
forks this functionThe program flow is controlled by the switch pid of
the child. The child Process performs execvp from \<unistd.h\> under
argv and and returns the exit status of that call. Normally it returns
SUCCESS through the exit in the mother process, otherwise, it returns
the code execution of execvp being taken from error\_codes (the error
command is recorded in this global variable defined in
\<error\_codes.h\>). If we can not process the execbin function, it
returns PROCFAIL.

#### For loop:

Parse checks starts by checking if the input command line starts with
for, it calls the forloop found in forhsh.c which parses it and calls
our execute function found in forhsh.c which calles parse again and
exechsh and execbin. This allows alot of nested operation to work. on
each iteration it calls parse with the new variable value initialisation
of the iterated item.

> for exemple if the for is iterating on i in 1 2 it would start by
> calling i=1 then executes its core, then i=2 and it executes.

#### Logic relations && and \|\| for exit status :

When Parse detects a && or \|\| inside an inputed command line that does
not start with a for, it calls checkExitStatus found in forhsh.c which
takes care of the parsing and the function calls depending on the logic
relation given, it calls our execute function found in forhsh.c which
calles parse again and exechsh and execbin. This allows alot of nested
operation to work. It also handles its errors internaly and it would
make Parse return SUCCES if the function excecuted with no shell
breaking error.

> Given exemples are the following:
>
> hashUp\> cat nofile && echo Le fichier existe. Output: cat: nofile: No
> such file or directory hashUp\> cat nofile \|\| echo Le fichier
> n'existe pas Output: cat: nofile: No such file or directory Le fichier
> n'existe pas

#### Error Handling :

The handling of errors arising from the parse functions, exechsh and
execbin done by handle\_parse\_error functions for parse errors and
handle\_command\_error for errors by exechsh and execbin. These
functions are implemented as we said in errhandler.c. The
handle\_parse\_error handles codes: BADTOKEN, DIRECTORY, NOFORDIR. The
handle\_command\_error handles codes: NOTFOUND, SEGMFAULT, NOPERMS,
NOFORDIR, PROCFAIL.

Extra Features :
----------------

#### SIGINT handling

Kills a subprocess/programms using Ctrl+c We found it useful to have to
debug and throughly test our program.

The HSH\_SIGINT function as defined in hshlib.h is the function that is
declared in the SIGINT callback event and informs that all processes
created by the shell should be killed if the SIGINT signal is sent.

