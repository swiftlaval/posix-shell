CC = gcc
OPTI = -O3 -finline-functions -fomit-frame-pointer -fno-strict-aliasing
STD = -std=gnu99
WARN = -Wmissing-prototypes -Wall
CCFLAGS = $(OPTI) $(STD)
DEBUG = -DDEBUG

INC = -Iinclude
SDIR = src
ODIR = build
LDIR = lib
LIB = -L$(LDIR) -lhsh
OUT = libhsh.a
EXECUTABLE = hashup
_SRCS = errhandler.c execbin.c exechsh.c hashMap.c forhsh.c
SOURCES = $(patsubst %,$(SDIR)/%,$(_SRCS))
_OBJS = $(_SRCS:.c=.o)
OBJECTS = $(patsubst %,$(ODIR)/%,$(_OBJS))

all: $(SOURCES) $(EXECUTABLE)

$(ODIR)/%.o: $(SDIR)/%.c
	if [ ! -d "$(ODIR)" ]; then mkdir $(ODIR); fi
	$(CC) $(CCFLAGS) -o $@ -c $< $(INC)

$(LDIR)/$(OUT): $(OBJECTS)
	if [ ! -d "$(LDIR)" ]; then mkdir $(LDIR); fi
	ar rvs $(LDIR)/$(OUT) $^

$(EXECUTABLE): $(SDIR)/hashup.c $(LDIR)/$(OUT)
	$(CC) $(CCFLAGS) -o $@ $< $(INC) $(LIB)

.PHONY: clean

clean:
	rm -rf $(ODIR)
