/**
 * @file hashup.c
 * @brief Contains the main function of hash-up shell software
 *
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 */

#include <stdio.h>
#include <signal.h>
#include "utils.h"
#include "hshlib.h"
#include "hashMap.h"

/**
 * @brief function to print a welcome screen to the user
 * @return void
 */
void welcome();

int main() {
  char buff[1000], cwd[1024];
  int argc;
  char *argv[64];
  int bg, status;
  hashMap *dict = createMap(10);
#ifdef DEBUGDICT
  int xi = 0;
#endif // DEBUGDICT

  welcome();

  signal(SIGINT, HSH_SIGINT);

  while (1) {

    fflush(stdout);
    fflush(stdin);

#ifdef DEBUGDICT
    printf("\n Dict after fflush: %d \n", xi);
    printMap(dict);
    xi = xi + 1;
    printf("---------------------\n");
#endif // DEBUGDICT

    if (getcwd(cwd, sizeof(cwd)) != NULL) {
      printf("%s:%s$\nhash it up >>> ", getenv("USER"),
             outputcwd(dict, cwd, argv, buff));
    } else {
      printf("hash it up >>> ");
    }

//!< read command

#ifdef DEBUGDICT

    printf("\n Dict after outputcwd: %d", xi);
    printMap(dict);
    xi = xi + 1;
    printf("\n");
#endif // DEBUGDICT

    SIGINTFLAG = 0;
    fgets(buff, 100, stdin);
    if (SIGINTFLAG)
      continue;
    //!< parse command
    if ((status = parse(dict, buff, &argc, argv, &bg)) != 0) {
      handle_parse_error(argv, status);
      continue;
    }
#ifdef DEBUG
    print(argv);
    printf("argc: %d\n", argc);
    //!< check for built-in shell command
    printf("Checking if it is a shell built-in cmd: %d\n", status);
#endif // DEBUG
    if ((status = exechsh(argc, argv)) == 0)
      continue;
//!< if built-in cmd not found, try for executables (-1 is cmd not found)
#ifdef DEBUG
    printf("Checking if it is a system executable: %d\n", status);
#endif // DEBUG
    if (status == NOBUILTIN) {
      //!< check for binary executable in PATH
      if ((status = execbin(argv, bg)) == 0)
        continue;
    }
    //!< output error if command was not found or executed unsuccessfully
    handle_command_error(argv, status);
  }
  return 0;
}

void welcome() {
  printf("<<<<<<<<<<<<           WELCOME to.....         >>>>>>>>>>>>\n");
  printf("██╗  ██╗ █████╗ ███████╗██╗  ██╗      ██╗   ██╗██████╗ ██╗\n"
         "██║  ██║██╔══██╗██╔════╝██║  ██║      ██║   ██║██╔══██╗██║\n"
         "███████║███████║███████╗███████║█████╗██║   ██║██████╔╝██║\n"
         "██╔══██║██╔══██║╚════██║██╔══██║╚════╝██║   ██║██╔═══╝ ╚═╝\n"
         "██║  ██║██║  ██║███████║██║  ██║      ╚██████╔╝██║     ██╗\n"
         "╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝       ╚═════╝ ╚═╝     ╚═╝\n"
         "                                                          \n\n");
  printf("Jad Yammine Moncef Fayjhi - Copyrights 2018\n\n\n");
}
