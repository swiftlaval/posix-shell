
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "forhsh.h"
#include "hshlib.h"

// array dynamique
// inspire de
// https://stackoverflow.com/questions/3536153/c-dynamically-growing-array

/******************************************************************************
 *                              For stucture                                  *
 ******************************************************************************/

typedef struct {
  char *array;
  size_t used;
  size_t size;
} CharArray;

typedef struct {
  CharArray *array;
  size_t used;
  size_t size;
} Array;

void initCharArray(CharArray *a, size_t initialSize) {
  a->array = malloc(initialSize * sizeof(char));
  a->used = 0;
  a->size = initialSize;
}

void initArray(Array *a, size_t initialSize) {
  a->array = (CharArray *)malloc(initialSize * sizeof(CharArray));

  a->used = 0;
  a->size = initialSize;
}

void insertCharArray(CharArray *a, char element) {
  if (a->used == a->size) {
    a->size *= 2;
    a->array = realloc(a->array, a->size * sizeof(char));
  }
  a->array[a->used++] = element;
}

void insertArray(Array *a, CharArray element) {
  if (a->used == a->size) {
    a->size *= 2;
    a->array = (CharArray *)realloc(a->array, a->size * sizeof(CharArray));
  }
  a->array[a->used++] = element;
}

void printCharArray(CharArray *a) {

  int j = 0;
  while (a->array[j] != '\0') {

    printf("%c", a->array[j]);
    j++;
  }
}

void freeCharArray(CharArray *a) {

  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}

void freeArray(Array *a) {
  free(a->array);

  a->array = NULL;
  a->used = a->size = 0;
}

void insertString(char *input, CharArray *a) {

  int i = 0;

  for (i; i < strlen(input); i++) {
    insertCharArray(a, input[i]);
  }
}

/******************************************************************************
 *                                execute                                     *
 ******************************************************************************/

void execute(char *buff, hashMap *dict, int *status) {
  char *newbuff = (char *)malloc(strlen(buff) + 1);
  strcpy(newbuff, buff);
  if (strcmp(newbuff, "") != 0) {
    // printf("--->>%s<<<\n",newbuff);
    strcat(newbuff, "\0");
    int bg, argc;
    argc = 0;
    bg = 0;

    char *argv[64];
    argv[0] = "";
    if ((*status = parse(dict, newbuff, &argc, argv, &bg)) != 0) {
      handle_parse_error(argv, *status);
    }
    if ((*status = exechsh(argc, argv)) == 0)
      return;
    if (*status == NOBUILTIN) {
      //!< check for binary executable in PATH
      if ((*status = execbin(argv, bg)) == 0)
        return;
    }

    handle_command_error(argv, *status);
  }
  free(newbuff);
}

/******************************************************************************
 *                                forloop                                     *
 ******************************************************************************/

void forloop(char *stringg, hashMap *dict, int *status) {

  char currStatus[5] = "for"; // status possible: for, in, do, done

  CharArray n[strlen(stringg)];
  CharArray itemName;

  Array commands;
  Array numbers;

  initArray(&commands, 1);
  initArray(&numbers, 1);
  int k = 0;
  for (int i = 0; i < strlen(stringg); i++) {
    // cette boucle organise les commandes et les items(1 2 3 dans l'exemple)
    // sur lequeles on ittere dans des array dynamique

    if ((stringg[i] == 'i') && (stringg[i + 1] == 'n') &&
        (stringg[i + 2] == ' ') && (strcmp(currStatus, "for") == 0)) { // si in
      strcpy(currStatus, "in");
      i = i + 3;
    } else if (stringg[i] == 'd' && stringg[i + 1] == 'o' &&
               stringg[i + 2] == ' ' &&
               strcmp(currStatus, "in") == 0) { // si do
      strcpy(currStatus, "do");
      i = i + 3;
    } else if (stringg[i] == 'd' && stringg[i + 1] == 'o' &&
               stringg[i + 2] == 'n' && stringg[i + 3] == 'e' &&
               strcmp(currStatus, "do") == 0) { // si do
      strcpy(currStatus, "done");
    }

    if (strcmp(currStatus, "for") == 0) {
      initCharArray(&itemName, 1);
      while (stringg[i] != ' ') {

        insertCharArray(&itemName, stringg[i]);

        i++;
      }
    }
    if (strcmp(currStatus, "in") == 0) {
      initCharArray(&n[k], 1);

      while (stringg[i] != ' ') {

        insertCharArray(&n[k], stringg[i]);

        i++;
      }
      insertArray(&numbers, n[k]);
      k++;
    }
    if (strcmp(currStatus, "do") == 0) {
      // on copie ce qui est apres le do, on s'arrete au ;
      initCharArray(&n[k], 1);
      while (stringg[i] != ';') {

        insertCharArray(&n[k], stringg[i]);

        i++;
      }
      insertArray(&commands, n[k]);
      k++;
    }
    if (strcmp(currStatus, "done") == 0) {
      break;
    }
  }

  for (int i = 0; i < numbers.used - 1; i++) {
    char temp1[itemName.size];
    char temp2[numbers.array[i].size];
    strcpy(temp1, itemName.array);
    strcpy(temp2, numbers.array[i].array);

    char temp3[sizeof(temp2) + 1];
    strcpy(temp3, temp1);
    strcat(temp3, "=");
    strcat(temp3, temp2);
    execute(temp3, dict, status);
    for (int j = 0; j < commands.used - 1; j++) {
      // char *temp4 = (char*) malloc(strlen(commands.array[j].array)+ 2);
      // strcat(commands.array[j].array,"\0");
      execute(commands.array[j].array, dict, status);
    }
  }

  // liberation de memoire
  freeCharArray(&itemName);

  for (int i = 0; i < numbers.used - 1; i++) {
    freeCharArray(&numbers.array[i]);
  }
  for (int j = 0; j < commands.used - 1; j++) {
    freeCharArray(&commands.array[j]);
  }
  freeArray(&numbers);
  freeArray(&commands);
}

/******************************************************************************
 *                           checkExitStatus                                  *
 ******************************************************************************/

void checkExitStatus(char *stringg, hashMap *dict, int *status) {
  // si la commande contient && ou || donner a cette fonction qui va gerer
  // la suite de l'execution
  int stat = 0; // 0 pour || , 1 pour &&
  char *token;
  char *leftPart = malloc(1);
  char *rightPart = malloc(1);

  for (int i = 0; i < strlen(stringg) - 1; i++) {
    if (stringg[i] == '|' && stringg[i + 1] == '|') {

      token = strtok(stringg, "||");
      leftPart = realloc(leftPart, strlen(token));
      strcpy(leftPart, token);

      token = strtok(NULL, "||");
      rightPart = realloc(rightPart, strlen(token));
      strcpy(rightPart, token);
      break;

    } else if (stringg[i] == '&' && stringg[i + 1] == '&') {

      token = strtok(stringg, "&&");
      leftPart = realloc(leftPart, strlen(token));
      strcpy(leftPart, token);
      stat = 1;

      token = strtok(NULL, "&&");
      rightPart = realloc(rightPart, strlen(token));
      strcpy(rightPart, token);
      break;
    }
  }

  if (stat == 0) { // ||
    execute(leftPart, dict, status);
    fflush(stdout);
    if (*status != 0) {
      execute(rightPart, dict, status);
    }
  }
  if (stat == 1) { // &&
    execute(leftPart, dict, status);
    fflush(stdout);
    if (*status == 0) {
      execute(rightPart, dict, status);
    }
  }
}