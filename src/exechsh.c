/**
 * @file exechsh.c
 * @brief Implements the definition of the exechsh function defined in exechsh.h
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 */

#include <string.h>
#include "utils.h"
#include "exechsh.h"

#define COMM_NUM 3 //!< number of built-in hash-up commands

/******************************************************************************
 *                           `   SHELL_COMMANDS                                *
 ******************************************************************************/

/**
 * @brief array of const c-strings containing the hash-up built-in commands
 * @todo shell commands as 'export', 'alias', 'source', etc
 */
char const *SHELL_COMMANDS[COMM_NUM] = {"cd", "exit", NULL};

/******************************************************************************
 *                                  exechsh                                   *
 ******************************************************************************/

int exechsh(int argc, char **argv) {
#ifdef DEBUG
  printf("in exechsh\n");
// print(SHELL_COMMANDS);
#endif // DEBUG
  int i, status;
  for (i = 0; i < COMM_NUM; ++i) {
    if (strcmp(argv[0], SHELL_COMMANDS[i]) == 0)
      break;
  }
  switch (i) {
  case 0:
    status = hshcd(argc, argv);
    break;

  case 1:
    status = hshexit(argc, argv);
    break;

  default:
    status = NOBUILTIN; // command not found
  }
  return status;
}
