/**
 * @file errhandler.c
 * @brief This file contains the implementation of error handling mechanisms
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 */

#include <stdio.h>
#include "hshlib.h"

/******************************************************************************
 *                         handle_parse_error                                 *
 ******************************************************************************/

void handle_parse_error(char **argv, int status) {
  switch (status) {
  case BADTOKEN:
    printf("hashup: syntax error near unexpected token '%s'\n", argv[0]);
    break;
  case DIRECTORY:
    printf("hashup: %s: Is a directory\n", argv[0]);
    break;
  case NOFORDIR:
    printf("hashup: %s: No such file or directory\n", argv[0]);
  }
}

/******************************************************************************
 *                         handle_command_error                               *
 ******************************************************************************/

void handle_command_error(char **argv, int status) {
  switch (status) {
  case NOTFOUND:
    printf("%s: command not found\n", argv[0]);
    break;
  case SEGMFAULT:
    printf("Segmentation fault (core dumped)\n");
    break;
  case NOPERMS:
    printf("hashup: %s: Permission denied\n", argv[0]);
    break;
  case NOFORDIR:
    printf("hashup: %s: %s: No such file or directory\n", argv[0], argv[1]);
    break;
  case PROCFAIL:
    printf("hashup: system failure unable to fork new process\n");
    break;
  }
}
