/**
 * @file execbin.c
 * @brief Contains the implentation of execbin function
 * @author Jad Yammine && Moncef Fayjha
 * @date February 2018
 * @version 0.01
 * @warning software not thoroughly tested
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include "hshlib.h"

/******************************************************************************
 *                                  execbin                                   *
 ******************************************************************************/

int execbin(char **argv, int bg) {
  /*
   * @todo Completely detaching process at background
   * @todo Stoping and resuming worker processes
   */
  pid_t child_pid, wpid;
  int status = 0, errsv = 0;
  switch (child_pid = fork()) {
  case -1:
    return PROCFAIL; // Failed to create new process.

  case 0:
// worker process is going to call the command
#ifdef DEBUG
    printf("Executing %s\n", argv[0]);
#endif // DEBUG
    if (execvp(*argv, argv) == -1) {
      errsv = errno;
#ifdef DEBUG
      printf("Process Error code was: %d\n", errsv);
#endif              // DEBUG
      _Exit(errsv); // Exit with error code
    }
    _Exit(SUCCESS); // Clean process exit

  default:
    /* Broker process is going to wait for the worker process to finish
     * if worker is to be working on the foreground
     */
    if (!bg) {
      do {
        wpid = waitpid(child_pid, &status, WUNTRACED);
        /* if (wpid == -1) {     */
        /*   perror("waitpid");  */
        /*   exit(EXIT_FAILURE); */
        /* }                     */
      } while (!WIFEXITED(status) && !WIFSIGNALED(status));
      if (WIFEXITED(status)) {
        errsv = WEXITSTATUS(status);
      } else if (WIFSIGNALED(status)) {
        errsv = WTERMSIG(status);
        printf("process [%d] killed (signal %d)\n", child_pid,
               WTERMSIG(status));
      }
#ifdef DEBUG
      printf("Broker from Process Error code was: %d\n", errsv);
#endif // DEBUG
    }
    return errsv;
  }
}
